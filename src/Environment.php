<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 04.03.2018
 * Time: 18:50
 */

namespace Blezigen\AcquiringSberbank;


use Blezigen\AcquiringSberbank\Type\Enum;

class Environment extends Enum
{
    const Production = 0;
    const Develop = 1;

    public static function GetEnvironmentUrl($environment)
    {
        if ($environment == self::Develop){
            return "https://3dsec.sberbank.ru/payment/rest/";
        }
        else if ($environment == self::Production){
            return "https://securepayments.sberbank.ru/payment/rest/";
        }
        else return "";
    }
}