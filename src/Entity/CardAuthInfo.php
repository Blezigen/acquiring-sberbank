<?php


namespace Blezigen\AcquiringSberbank\Entity;


class CardAuthInfo
{
    protected $approvalCode;
    protected $pan;
    protected $maskedPan;
    protected $expiration;
    protected $cardholderName;
    protected $chargeback;
    protected $paymentSystem;
    protected $product;
    protected $paymentWay;

    /**
     * @return mixed
     */
    public function getApprovalCode()
    {
        return $this->approvalCode;
    }

    /**
     * @param mixed $approvalCode
     */
    public function setApprovalCode($approvalCode)
    {
        $this->approvalCode = $approvalCode;
    }

    /**
     * @return mixed
     */
    public function getPan()
    {
        return $this->pan;
    }

    /**
     * @param mixed $pan
     */
    public function setPan($pan)
    {
        $this->pan = $pan;
    }

    /**
     * @return mixed
     */
    public function getMaskedPan()
    {
        return $this->maskedPan;
    }

    /**
     * @param mixed $maskedPan
     */
    public function setMaskedPan($maskedPan)
    {
        $this->maskedPan = $maskedPan;
    }

    /**
     * @return mixed
     */
    public function getExpiration()
    {
        return $this->expiration;
    }

    /**
     * @param mixed $expiration
     */
    public function setExpiration($expiration)
    {
        $this->expiration = $expiration;
    }

    /**
     * @return mixed
     */
    public function getCardholderName()
    {
        return $this->cardholderName;
    }

    /**
     * @param mixed $cardholderName
     */
    public function setCardholderName($cardholderName)
    {
        $this->cardholderName = $cardholderName;
    }

    /**
     * @return mixed
     */
    public function getChargeback()
    {
        return $this->chargeback;
    }

    /**
     * @param mixed $chargeback
     */
    public function setChargeback($chargeback)
    {
        $this->chargeback = $chargeback;
    }

    /**
     * @return mixed
     */
    public function getPaymentSystem()
    {
        return $this->paymentSystem;
    }

    /**
     * @param mixed $paymentSystem
     */
    public function setPaymentSystem($paymentSystem)
    {
        $this->paymentSystem = $paymentSystem;
    }

    /**
     * @return mixed
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param mixed $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }

    /**
     * @return mixed
     */
    public function getPaymentWay()
    {
        return $this->paymentWay;
    }

    /**
     * @param mixed $paymentWay
     */
    public function setPaymentWay($paymentWay)
    {
        $this->paymentWay = $paymentWay;
    }


}