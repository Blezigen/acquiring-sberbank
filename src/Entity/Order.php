<?php
namespace Blezigen\AcquiringSberbank\Entity;
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 04.03.2018
 * Time: 16:12
 */
class Order
{
    private $orderId = "";

    private $orderNumber = "";

    private $amount = 0;
    private $description = "";
    private $params = "";

    /**
     * @return mixed
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return mixed
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * @param $orderNumber
     */
    public function setOrderNumber($orderNumber)
    {
        $this->orderNumber = $orderNumber;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @return array
     */
    public function getJsonParams()
    {
        return json_encode($this->params);
    }

    /**
     * @param array $Params
     */
    public function setParams($Params)
    {
        $this->params = $Params;
    }

    /**
     * @param string $Params
     */
    public function setJsonParams($Params)
    {
        $this->params = json_decode($Params,true);
    }



}