<?php


namespace Blezigen\AcquiringSberbank\Entity;


class BindingInfo
{
    protected $clientId;
    protected $bindingId;
    protected $authDateTime;
    protected $authRefNum;
    protected $terminalId;

    /**
     * @return mixed
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @param mixed $clientId
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
    }

    /**
     * @return mixed
     */
    public function getBindingId()
    {
        return $this->bindingId;
    }

    /**
     * @param mixed $bindingId
     */
    public function setBindingId($bindingId)
    {
        $this->bindingId = $bindingId;
    }

    /**
     * @return mixed
     */
    public function getAuthDateTime()
    {
        return $this->authDateTime;
    }

    /**
     * @param mixed $authDateTime
     */
    public function setAuthDateTime($authDateTime)
    {
        $this->authDateTime = $authDateTime;
    }

    /**
     * @return mixed
     */
    public function getAuthRefNum()
    {
        return $this->authRefNum;
    }

    /**
     * @param mixed $authRefNum
     */
    public function setAuthRefNum($authRefNum)
    {
        $this->authRefNum = $authRefNum;
    }

    /**
     * @return mixed
     */
    public function getTerminalId()
    {
        return $this->terminalId;
    }

    /**
     * @param mixed $terminalId
     */
    public function setTerminalId($terminalId)
    {
        $this->terminalId = $terminalId;
    }


}