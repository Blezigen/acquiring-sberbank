<?php
namespace Blezigen\AcquiringSberbank\Entity;


use Blezigen\AcquiringSberbank\Currency;
use Blezigen\AcquiringSberbank\Environment;
use Blezigen\AcquiringSberbank\HttpMethod;
use Blezigen\AcquiringSberbank\Language;
use Blezigen\AcquiringSberbank\PageView;

class Setting
{
    const DateFormat = 'YmdHis';

    private $environment;

    private $username;
    private $password;
    private $httpMethod;
    private $language;
    private $currency;
    private $pageView;

    private $returnUrl;
    private $failedUrl;

    private $sessionTimeOutSecond;

    /**
     * Setting constructor.
     */
    public function __construct()
    {
        $this->environment = Environment::Production;
        $this->username = "";
        $this->password = "";
        $this->returnUrl = "http://example.com/success";
        $this->failedUrl = "http://example.com/filed";
        $this->sessionTimeOutSecond = 60;
        $this->httpMethod = HttpMethod::POST;
        $this->currency = Currency::RUB;
        $this->pageView = PageView::Desktop;
        $this->language = Language::Russian;
    }


    /**
     * @return string
     */
    public function getEnvironment()
    {
        return $this->environment;
    }

    /**
     * @param $environment
     */
    public function setEnvironment($environment)
    {
        if (Environment::isDefinedValue($environment))
            $this->environment = $environment;
        else
            throw new \InvalidArgumentException("Allowed environment ".Environment::Production." or ".Environment::Develop);
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getHttpMethod()
    {
        return $this->httpMethod;
    }

    /**
     * @param string $httpMethod
     */
    public function setHttpMethod($httpMethod)
    {
        if (HttpMethod::isDefinedValue($httpMethod))
            $this->httpMethod = $httpMethod;
        else
            throw new \InvalidArgumentException("Allowed method GET or POST");
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param string $language
     */
    public function setLanguage($language)
    {
        if (Language::isDefinedValue($language))
            $this->language = $language;
        else
            throw new \InvalidArgumentException("Language ".$language." is not defined ISO 639-1");
    }

    /**
     * @return int
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param int $currency
     */
    public function setCurrency($currency)
    {
        if (Currency::isDefinedValue($currency))
            $this->currency = $currency;
        else
            throw new \InvalidArgumentException("Currency ".$currency." is not defined ISO 4217");
    }

    /**
     * @return string
     */
    public function getPageView()
    {
        return $this->pageView;
    }

    /**
     * @param string $pageView
     */
    public function setPageView($pageView)
    {
        if (PageView::isDefinedValue($pageView))
            $this->pageView = $pageView;
        else
            throw new \InvalidArgumentException("PageView ".$pageView." is not defined");
    }

    /**
     * @return string
     */
    public function getReturnUrl()
    {
        return $this->returnUrl;
    }

    /**
     * @param string $returnUrl
     */
    public function setReturnUrl($returnUrl)
    {
        $this->returnUrl = $returnUrl;
    }

    /**
     * @return string
     */
    public function getFailedUrl()
    {
        return $this->failedUrl;
    }

    /**
     * @param string $failedUrl
     */
    public function setFailedUrl($failedUrl)
    {
        $this->failedUrl = $failedUrl;
    }

    /**
     * @return int
     */
    public function getSessionTimeOutSecond()
    {
        return $this->sessionTimeOutSecond;
    }

    /**
     * @param int $sessionTimeOutSecond
     */
    public function setSessionTimeOutSecond($sessionTimeOutSecond)
    {
        $this->sessionTimeOutSecond = $sessionTimeOutSecond;
    }
}