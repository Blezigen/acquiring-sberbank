<?php


namespace Blezigen\AcquiringSberbank\Entity;


class PaymentAmountInfo
{
    protected $paymentState;
    protected $approvedAmount;
    protected $depositedAmount;
    protected $refundedAmount;
    protected $feeAmount;

    /**
     * @return mixed
     */
    public function getPaymentState()
    {
        return $this->paymentState;
    }

    /**
     * @param mixed $paymentState
     */
    public function setPaymentState($paymentState)
    {
        $this->paymentState = $paymentState;
    }

    /**
     * @return mixed
     */
    public function getApprovedAmount()
    {
        return $this->approvedAmount;
    }

    /**
     * @param mixed $approvedAmount
     */
    public function setApprovedAmount($approvedAmount)
    {
        $this->approvedAmount = $approvedAmount;
    }

    /**
     * @return mixed
     */
    public function getDepositedAmount()
    {
        return $this->depositedAmount;
    }

    /**
     * @param mixed $depositedAmount
     */
    public function setDepositedAmount($depositedAmount)
    {
        $this->depositedAmount = $depositedAmount;
    }

    /**
     * @return mixed
     */
    public function getRefundedAmount()
    {
        return $this->refundedAmount;
    }

    /**
     * @param mixed $refundedAmount
     */
    public function setRefundedAmount($refundedAmount)
    {
        $this->refundedAmount = $refundedAmount;
    }

    /**
     * @return mixed
     */
    public function getFeeAmount()
    {
        return $this->feeAmount;
    }

    /**
     * @param mixed $feeAmount
     */
    public function setFeeAmount($feeAmount)
    {
        $this->feeAmount = $feeAmount;
    }


}
