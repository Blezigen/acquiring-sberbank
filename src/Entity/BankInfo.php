<?php


namespace Blezigen\AcquiringSberbank\Entity;


class BankInfo
{
    protected $bankName;
    protected $bankCountryCode;
    protected $bankCountryName;

    /**
     * @return mixed
     */
    public function getBankName()
    {
        return $this->bankName;
    }

    /**
     * @param mixed $bankName
     */
    public function setBankName($bankName)
    {
        $this->bankName = $bankName;
    }

    /**
     * @return mixed
     */
    public function getBankCountryCode()
    {
        return $this->bankCountryCode;
    }

    /**
     * @param mixed $bankCountryCode
     */
    public function setBankCountryCode($bankCountryCode)
    {
        $this->bankCountryCode = $bankCountryCode;
    }

    /**
     * @return mixed
     */
    public function getBankCountryName()
    {
        return $this->bankCountryName;
    }

    /**
     * @param mixed $bankCountryName
     */
    public function setBankCountryName($bankCountryName)
    {
        $this->bankCountryName = $bankCountryName;
    }



}