<?php

namespace Blezigen\AcquiringSberbank\Extend;

use Exception;

class ExtendMethods
{
    /**
     * @param $text
     * @return string
     */
    public static function normalizedCaseString($text){
        return strtolower(substr($text, 0, 1)) . substr($text, 1);
    }

    /**
     * @param $array
     * @return array
     * @throws Exception
     */
    public static function normalizedKeys($array)
    {
        $newArray = array();
        foreach ($array as $key => $value) {
            $key = ExtendMethods::normalizedCaseString($key);
            if (!array_key_exists($key, $newArray)){
                $newArray[$key] = $value;
            } else {
                throw new Exception('Duplicate Key Encountered');
            }
        }
        return $newArray;
    }
}