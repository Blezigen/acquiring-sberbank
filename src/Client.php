<?php
namespace Blezigen\AcquiringSberbank;

use Blezigen\AcquiringSberbank\Client\AbstractLoader;
use Blezigen\AcquiringSberbank\Client\ApiVersionRest;
use Blezigen\AcquiringSberbank\Entity\Setting;

class Client {

    /**
     * @var AbstractLoader $request;
     */
    private $request;
    /**
     * @var Setting $setting;
     */
    private $setting;

    /**
     * Constructor.
     *
     * @param Setting $setting
     * @internal param array $settings Client's settings
     */
    public function __construct($setting)
    {
        if (!extension_loaded('json')) {
            throw new \RuntimeException('JSON extension is not loaded.');
        }

        $this->setting = $setting;

        $this->request = new ApiVersionRest($setting);
    }

    public function getRequest()
    {
        return $this->request;
    }

}