<?php
namespace Blezigen\AcquiringSberbank;

use Blezigen\AcquiringSberbank\Type\Enum;

/**
 * A language code in ISO 639-1 format ('en', 'ru' and etc.).
 *
 * @author Oleg Voronkovich <oleg-voronkovich@yandex.ru>
 * @see https://en.wikipedia.org/wiki/ISO_639-1
 */
class PageView extends Enum
{
    const Desktop = "DESKTOP";
    const Mobile = "MOBILE";

    const IPhone = "iphone";
}