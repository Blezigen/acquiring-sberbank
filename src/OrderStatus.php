<?php
namespace Blezigen\AcquiringSberbank;

use Blezigen\AcquiringSberbank\Type\Enum;


class OrderStatus extends Enum
{
    //0 Заказ зарегистрирован, но не оплачен
    const FULL_SUCCESS = 0;
    //1 Предавторизованная сумма захолдирована (для двухстадийных платежей)
    const HOLD_SUCCESS = 1;
    //2 Проведена полная авторизация суммы заказа
    const FULL_HOLD_SUCCESS = 2;
    //3 Авторизация отменена
    const AUTH_FILED = 3;
    //4 По транзакции была проведена операция возврата
    const TRANSACTION_RETURN = 4;
    //5 Инициирована авторизация через ACS банка-эмитента
    const AUTHORIZATION_ACS_BANK = 5;
    //6 Авторизация отклонена
    const AUTHORIZATION_FILED = 6;

}