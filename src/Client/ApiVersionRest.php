<?php

namespace Blezigen\AcquiringSberbank\Client;

use Blezigen\AcquiringSberbank\Methods\MethodDeposit;
use Blezigen\AcquiringSberbank\Methods\MethodGetOrderStatus;
use Blezigen\AcquiringSberbank\Methods\MethodGetOrderStatusExtended;
use Blezigen\AcquiringSberbank\Methods\MethodRefund;
use Blezigen\AcquiringSberbank\Methods\MethodRegister;
use Blezigen\AcquiringSberbank\Methods\MethodRegisterPreAuth;
use Blezigen\AcquiringSberbank\Methods\MethodReverse;

class ApiVersionRest extends AbstractLoader
{
    use MethodGetOrderStatusExtended;
    use MethodGetOrderStatus;
    use MethodRegisterPreAuth;
    use MethodRegister;
    use MethodReverse;
    use MethodRefund;
    use MethodDeposit;

    public function __construct($setting)
    {
        parent::__construct($setting);
    }

}