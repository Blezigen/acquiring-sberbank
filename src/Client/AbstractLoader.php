<?php
namespace Blezigen\AcquiringSberbank\Client;

use Blezigen\AcquiringSberbank\Entity\Setting;
use Blezigen\AcquiringSberbank\Environment;
use Blezigen\AcquiringSberbank\Http\HttpClient;

class AbstractLoader
{
    /**
     * @var Setting $setting
     */
    protected $setting;

    /**
     * @var HttpClient
     */
    protected $httpClient;

    /**
     * Init version based client
     * @param Setting $setting
     */
    public function __construct($setting)
    {
        $this->setting = $setting;
        $this->httpClient = new HttpClient(
            Environment::GetEnvironmentUrl(
                $this->setting->getEnvironment()
            ),
            [
                'userName' => $this->setting->getUsername(),
                'password' => $this->setting->getPassword(),
                'currency' => $this->setting->getCurrency(),
                'language' => $this->setting->getLanguage(),
                'pageView' => $this->setting->getPageView()
            ]
        );
    }
}