<?php

namespace Blezigen\AcquiringSberbank\Exception;

class ResponseException extends \Exception
{
    /**
     * BaseResponseException constructor.
     * @param string $code
     * @param string $message
     */
    public function __construct($code, $message)
    {
        parent::__construct($message, $code);
    }
}