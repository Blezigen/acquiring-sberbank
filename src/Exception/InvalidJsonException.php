<?php
namespace Blezigen\AcquiringSberbank\Exception;

/**
 * Class InvalidJsonException
 * @package Blezigen\Acquiring\Sberbank\Exception
 */
class InvalidJsonException extends \DomainException
{
}