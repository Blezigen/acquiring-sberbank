<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 04.03.2018
 * Time: 21:11
 */

namespace Blezigen\AcquiringSberbank\Exception;

class ErrorResponseException extends \Exception
{
    protected $code;
    protected $message;

    /**
     * ErrorResponseException constructor.
     */
    public function __construct()
    {
    }
}