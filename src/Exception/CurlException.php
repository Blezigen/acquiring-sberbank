<?php

namespace Blezigen\AcquiringSberbank\Exception;

/**
 * Class CurlException
 * @package Blezigen\Acquiring\Sberbank\Exception
 */
class CurlException extends \RuntimeException
{
}