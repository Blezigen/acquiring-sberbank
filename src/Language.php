<?php

namespace Blezigen\AcquiringSberbank;

use Blezigen\AcquiringSberbank\Type\Enum;

class Language  extends Enum
{
    const Armenian = "hy";
    const Chinese = "zh";
    const Czech = "cs";
    const Danish = "da";
    const Dutch = "nl";
    const English = "en";
    const Esperanto = "eo";
    const Finnish = "fi";
    const French = "fr";
    const Georgian = "ka";
    const German = "de";
    const Greek = "el";
    const Italian = "it";
    const Japanese = "ja";
    const Korean = "ko";
    const Kurdish = "ku";
    const Persian = "fa";
    const Polish = "pl";
    const Portuguese = "pt";
    const Romanian = "ro";
    const Russian = "ru";
    const Spanish = "es";
    const Swedish = "sv";
    const Turkish = "tr";
    const Urdu = "ur";

}