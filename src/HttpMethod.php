<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 04.03.2018
 * Time: 18:17
 */

namespace Blezigen\AcquiringSberbank;

use Blezigen\AcquiringSberbank\Type\Enum;

class HttpMethod extends Enum
{
    const GET = "GET";
    const POST = "POST";
}