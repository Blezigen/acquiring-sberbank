<?php
namespace Blezigen\AcquiringSberbank\Response;

class ResponseRegister extends BasicResponse
{
    const ORDER_EXIST_CODE = 1;
    const UNKNOWN_CURRENCY_CODE = 3;
    const ABSENT_ARGUMENT_CODE = 4;
    const VALUE_ARGUMENT_CODE = 5;
    const SYSTEM_ERROR_CODE = 7;

    protected $orderId;
    protected $formUrl;

    /**
     * @return mixed
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param mixed $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return mixed
     */
    public function getFormUrl()
    {
        return $this->formUrl;
    }

    /**
     * @param mixed $formUrl
     */
    public function setFormUrl($formUrl)
    {
        $this->formUrl = $formUrl;
    }
}