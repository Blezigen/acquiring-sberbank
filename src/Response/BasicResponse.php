<?php
namespace Blezigen\AcquiringSberbank\Response;

/**
 * Class BasicResponse
 * @package Blezigen\Acquiring\Sberbank\Response
 */
class BasicResponse
{
    const SUCCESS_CODE = "0";

    private $code;
    private $response;

    protected $errorCode = self::SUCCESS_CODE;
    protected $errorMessage = "";


    //region [GET&SET] methods

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param mixed $response
     */
    public function setResponse($response)
    {
        $this->response = $response;
    }

    /**
     * @return string
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @param string $errorCode
     */
    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * @param string $errorMessage
     */
    public function setErrorMessage( $errorMessage)
    {
        $this->errorMessage = $errorMessage;
    }

    //endregion

    /**
     * @param array $array
     */
    public function cast($array)
    {
        $className = get_class($this);
        $methods = get_class_methods($className);

        foreach ($methods as $method) {

            preg_match(' /^(set)(.*?)$/i', $method, $results);
            $pre = array_key_exists(1, $results) ? $results[1] : '';
            $k = array_key_exists(2, $results) ? $results[2] : '';

            $k = strtolower(substr($k, 0, 1)) . substr($k, 1);

            If ($pre == 'set' && !empty($array[$k])) {

                $this->$method($array[$k]);
            }
        }
    }

}