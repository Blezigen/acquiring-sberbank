<?php

namespace Blezigen\AcquiringSberbank\Response;

use Blezigen\AcquiringSberbank\Entity\Attribute;
use Blezigen\AcquiringSberbank\Entity\BankInfo;
use Blezigen\AcquiringSberbank\Entity\BindingInfo;
use Blezigen\AcquiringSberbank\Entity\CardAuthInfo;
use Blezigen\AcquiringSberbank\Entity\PaymentAmountInfo;
use Blezigen\AcquiringSberbank\Entity\SecureAuthInfo;
use Blezigen\AcquiringSberbank\Extend\ExtendMethods;

class ResponseOrderStatusExtended extends BasicResponse
{
    /** @var string $orderNumber */
    protected $orderNumber;

    /** @var int $orderStatus */
    protected $orderStatus;

    /** @var int $actionCode */
    protected $actionCode = 0;

    /** @var string $actionCodeDescription */
    protected $actionCodeDescription = "";

    /** @var int $amount */
    protected $amount;

    /** @var string $currency */
    protected $currency;

    /** @var int $date */
    protected $date;

    /** @var string $ip */
    protected $ip;

    /** @var int $authDateTime */
    protected $authDateTime;

    /** @var string $terminalId */
    protected $terminalId;

    /** @var string $authRefNum */
    protected $authRefNum;

    /** @var Attribute[] $merchantOrderParams */
    protected $merchantOrderParams;

    /** @var Attribute[] $merchantOrderParams */
    protected $attributes;

    /** @var BindingInfo $bindingInfo */
    protected $bindingInfo;

    /** @var SecureAuthInfo $secureAuthInfo */
    protected $secureAuthInfo;

    /** @var CardAuthInfo $cardAuthInfo */
    protected $cardAuthInfo;

    /** @var  PaymentAmountInfo $paymentAmountInfo */
    protected $paymentAmountInfo;

    /** @var  BankInfo $bankInfo */
    protected $bankInfo;

    //region [Get] and [Set] Methods

    /**
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * @param string $errorMessage
     */
    public function setErrorMessage($errorMessage)
    {
        $this->errorMessage = $errorMessage;
    }

    /**
     * @return string
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * @param string $orderNumber
     */
    public function setOrderNumber($orderNumber)
    {
        $this->orderNumber = $orderNumber;
    }

    /**
     * @return int
     */
    public function getOrderStatus()
    {
        return $this->orderStatus;
    }

    /**
     * @param int $orderStatus
     */
    public function setOrderStatus($orderStatus)
    {
        $this->orderStatus = $orderStatus;
    }

    /**
     * @return int
     */
    public function getActionCode()
    {
        return $this->actionCode;
    }

    /**
     * @param int $actionCode
     */
    public function setActionCode($actionCode)
    {
        $this->actionCode = $actionCode;
    }

    /**
     * @return string
     */
    public function getActionCodeDescription()
    {
        return $this->actionCodeDescription;
    }

    /**
     * @param string $actionCodeDescription
     */
    public function setActionCodeDescription($actionCodeDescription)
    {
        $this->actionCodeDescription = $actionCodeDescription;
    }

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return int
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param int $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    }

    /**
     * @return int
     */
    public function getAuthDateTime()
    {
        return $this->authDateTime;
    }

    /**
     * @param int $authDateTime
     */
    public function setAuthDateTime($authDateTime)
    {
        $this->authDateTime = $authDateTime;
    }

    /**
     * @return string
     */
    public function getTerminalId()
    {
        return $this->terminalId;
    }

    /**
     * @param string $terminalId
     */
    public function setTerminalId($terminalId)
    {
        $this->terminalId = $terminalId;
    }

    /**
     * @return string
     */
    public function getAuthRefNum()
    {
        return $this->authRefNum;
    }

    /**
     * @param string $authRefNum
     */
    public function setAuthRefNum($authRefNum)
    {
        $this->authRefNum = $authRefNum;
    }

    /**
     * @return Attribute[]
     */
    public function getMerchantOrderParams()
    {
        return $this->merchantOrderParams;
    }

    /**
     * @param Attribute[] $merchantOrderParams
     */
    public function setMerchantOrderParams($merchantOrderParams)
    {
        $this->merchantOrderParams = $merchantOrderParams;
    }

    /**
     * @return Attribute[]
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @param Attribute[] $attributes
     */
    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;
    }

    /**
     * @return CardAuthInfo
     */
    public function getCardAuthInfo()
    {
        return $this->cardAuthInfo;
    }

    /**
     * @param CardAuthInfo $cardAuthInfo
     */
    public function setCardAuthInfo($cardAuthInfo)
    {
        $this->cardAuthInfo = $cardAuthInfo;
    }

    /**
     * @return PaymentAmountInfo
     */
    public function getPaymentAmountInfo()
    {
        return $this->paymentAmountInfo;
    }

    /**
     * @param PaymentAmountInfo $paymentAmountInfo
     */
    public function setPaymentAmountInfo($paymentAmountInfo)
    {
        $this->paymentAmountInfo = $paymentAmountInfo;
    }

    /**
     * @return BankInfo
     */
    public function getBankInfo()
    {
        return $this->bankInfo;
    }

    /**
     * @param BankInfo $bankInfo
     */
    public function setBankInfo($bankInfo)
    {
        $this->bankInfo = $bankInfo;
    }

    /**
     * @return SecureAuthInfo
     */
    public function getSecureAuthInfo()
    {
        return $this->secureAuthInfo;
    }

    /**
     * @param SecureAuthInfo $secureAuthInfo
     */
    public function setSecureAuthInfo($secureAuthInfo)
    {
        $this->secureAuthInfo = $secureAuthInfo;
    }

    /**
     * @return BindingInfo
     */
    public function getBindingInfo()
    {
        return $this->bindingInfo;
    }

    /**
     * @param BindingInfo $bindingInfo
     */
    public function setBindingInfo($bindingInfo)
    {
        $this->bindingInfo = $bindingInfo;
    }


    //endregion


    /**
     * @param array $array
     */
    public function cast($array)
    {
        $array = ExtendMethods::normalizedKeys($array);
        $className = get_class($this);
        $methods = get_class_methods($className);

        foreach ($methods as $method) {

            preg_match(' /^(set)(.*?)$/i', $method, $results);
            $pre = array_key_exists(1, $results) ? $results[1] : '';
            $k = array_key_exists(2, $results) ? $results[2] : '';

            $k = ExtendMethods::normalizedCaseString($k);

            If ($pre == 'set' && !empty($array[$k])) {
                switch ($k){
                    case "merchantOrderParams":

                        foreach ($array[$k] as $data)
                            $this->merchantOrderParams[] = new Attribute($data["name"],$data["value"]);

                        continue;
                    case "attributes":

                        foreach ($array[$k] as $data) {
                            $this->attributes[] = new Attribute($data["name"],$data["value"]);
                        }
                        continue;
                    case "cardAuthInfo":
                        $data = $array[$k];
                        $temp = new CardAuthInfo();

                        $temp->setExpiration($data["expiration"]);
                        $temp->setCardholderName($data["cardholderName"]);
                        $temp->setApprovalCode($data["approvalCode"]);
                        $temp->setChargeback($data["chargeback"]);
                        $temp->setMaskedPan($data["maskedPan"]);
                        $temp->setPan($data["pan"]);
                        $temp->setPaymentSystem($data["paymentSystem"]);
                        $temp->setPaymentWay($data["paymentWay"]);
                        $temp->setProduct($data["product"]);

                        $this->setCardAuthInfo($temp);
                        continue;
                    case "paymentAmountInfo":
                        $data = $array[$k];

                        $temp = new PaymentAmountInfo();

                        $temp->setPaymentState($data["paymentState"]);
                        $temp->setApprovedAmount($data["approvedAmount"]);
                        $temp->setDepositedAmount($data["depositedAmount"]);
                        $temp->setRefundedAmount($data["refundedAmount"]);
                        $temp->setFeeAmount($data["feeAmount"]);

                        $this->setPaymentAmountInfo($temp);
                        continue;
                    case "bankInfo":
                        $data = $array[$k];
                        $temp = new BankInfo();

                        $temp->setBankName($data["bankName"]);
                        $temp->setBankCountryName($data["bankCountryName"]);
                        $temp->setBankCountryCode($data["bankCountryCode"]);

                        $this->setBankInfo($temp);
                        continue;
                    case "bindingInfo":
                        $data = $array[$k];
                        $temp = new BindingInfo();

                        $temp->setTerminalId($data["terminalId"]);
                        $temp->setClientId($data["clientId  "]);
                        $temp->setBindingId($data["bindingId"]);
                        $temp->setAuthRefNum($data["authRefNum"]);
                        $temp->setAuthDateTime($data["authDateTime"]);

                        $this->setBindingInfo($temp);

                        continue;
                    case "secureAuthInfo":
                        $data = $array[$k];
                        $temp = new SecureAuthInfo();

                        $temp->setCavv($data["cavv"]);
                        $temp->setEci($data["eci"]);
                        $temp->setXid($data["xid"]);

                        $this->setSecureAuthInfo($temp);

                        continue;
                    default:
                        $this->$method($array[$k]);
                        break;
                }
            }
        }
    }
}