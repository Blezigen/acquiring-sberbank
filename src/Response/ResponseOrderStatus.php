<?php

namespace Blezigen\AcquiringSberbank\Response;

use Blezigen\AcquiringSberbank\Entity\BindingInfo;
use Blezigen\AcquiringSberbank\Extend\ExtendMethods;

class ResponseOrderStatus extends BasicResponse
{

    protected $expiration;
    protected $cardholderName;
    protected $depositAmount;
    protected $currency;
    protected $approvalCode;
    protected $authCode;
    protected $orderStatus;
    protected $orderNumber;
    protected $pan;
    protected $amount;
    protected $ip;

    /** @var BindingInfo */
    protected $bindingInfo;


    //region [GET] and [SET] methods

    /**
     * @return mixed
     */
    public function getExpiration()
    {
        return $this->expiration;
    }

    /**
     * @param mixed $expiration
     */
    public function setExpiration($expiration)
    {
        $this->expiration = $expiration;
    }

    /**
     * @return mixed
     */
    public function getCardholderName()
    {
        return $this->cardholderName;
    }

    /**
     * @param mixed $cardholderName
     */
    public function setCardholderName($cardholderName)
    {
        $this->cardholderName = $cardholderName;
    }

    /**
     * @return mixed
     */
    public function getDepositAmount()
    {
        return $this->depositAmount;
    }

    /**
     * @param mixed $depositAmount
     */
    public function setDepositAmount($depositAmount)
    {
        $this->depositAmount = $depositAmount;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return mixed
     */
    public function getApprovalCode()
    {
        return $this->approvalCode;
    }

    /**
     * @param mixed $approvalCode
     */
    public function setApprovalCode($approvalCode)
    {
        $this->approvalCode = $approvalCode;
    }

    /**
     * @return mixed
     */
    public function getAuthCode()
    {
        return $this->authCode;
    }

    /**
     * @param mixed $authCode
     */
    public function setAuthCode($authCode)
    {
        $this->authCode = $authCode;
    }

    /**
     * @return mixed
     */
    public function getOrderStatus()
    {
        return $this->orderStatus;
    }

    /**
     * @param mixed $orderStatus
     */
    public function setOrderStatus($orderStatus)
    {
        $this->orderStatus = $orderStatus;
    }

    /**
     * @return mixed
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * @param mixed $orderNumber
     */
    public function setOrderNumber($orderNumber)
    {
        $this->orderNumber = $orderNumber;
    }

    /**
     * @return mixed
     */
    public function getPan()
    {
        return $this->pan;
    }

    /**
     * @param mixed $pan
     */
    public function setPan($pan)
    {
        $this->pan = $pan;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param mixed $ip
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    }

    /**
     * @return BindingInfo
     */
    public function getBindingInfo()
    {
        return $this->bindingInfo;
    }

    /**
     * @param BindingInfo $bindingInfo
     */
    public function setBindingInfo($bindingInfo)
    {
        $this->bindingInfo = $bindingInfo;
    }

    //endregion


    /**
     * @param array $array
     */
    public function cast($array)
    {
        $array = ExtendMethods::normalizedKeys($array);
        $className = get_class($this);
        $methods = get_class_methods($className);

        foreach ($methods as $method) {

            preg_match(' /^(set)(.*?)$/i', $method, $results);

            $pre = array_key_exists(1, $results) ? $results[1] : '';
            $k = array_key_exists(2, $results) ? $results[2] : '';

            $k = ExtendMethods::normalizedCaseString($k);

            If ($pre == 'set' && !empty($array[$k])) {
                switch ($k) {
                    case "bindingInfo":
                        $data = $array[$k];

                        $temp = new BindingInfo();
                        $temp->setAuthDateTime($data["authDateTime"]);
                        $temp->setAuthRefNum($data["authRefNum"]);
                        $temp->setBindingId($data["bindingId"]);
                        $temp->setClientId($data["clientId"]);
                        $temp->setTerminalId($data["terminalId"]);

                        $this->setBindingInfo($temp);
                        continue;
                    default:
                        $this->$method($array[$k]);
                        break;
                }
            }
        }
    }
}