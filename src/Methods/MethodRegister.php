<?php

namespace Blezigen\AcquiringSberbank\Methods;

use Blezigen\AcquiringSberbank\Entity\Order;
use Blezigen\AcquiringSberbank\Entity\Setting;
use Blezigen\AcquiringSberbank\Exception\ResponseException;
use Blezigen\AcquiringSberbank\Http\HttpClient;
use Blezigen\AcquiringSberbank\Response\ResponseRegister;

trait MethodRegister
{
    /**
     * @param Order $order
     * @return ResponseRegister
     * @throws ResponseException
     */
    public function register(Order $order)
    {
        /** @var HttpClient $httpClient */
        $httpClient = $this->httpClient;
        /** @var Setting $setting */
        $setting = $this->setting;

        $params = [
            "orderNumber" => $order->getOrderNumber(),
            "amount" => $order->getAmount(),
            "description" => $order->getDescription(),

            "returnUrl" => $setting->getReturnUrl(),
            "failUrl" => $setting->getFailedUrl(),

            "sessionTimeoutSecs" => $setting->getSessionTimeOutSecond(),
        ];

        if (!empty($order->getParams()))
            $params["jsonParams"] = $order->getJsonParams();

        /** @var ResponseRegister $response **/
        $response = $httpClient->makeRequest(
            "register.do",
            $setting->getHttpMethod(),
            $params,
            false,
            ResponseRegister::class
        );

        if ($response->getErrorCode() != 0) {
            throw new ResponseException($response->getErrorCode(), $response->getErrorMessage());
        }

        return $response;
    }
}