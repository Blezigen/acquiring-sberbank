<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 05.03.2018
 * Time: 23:55
 */

namespace Blezigen\AcquiringSberbank\Methods;


use Blezigen\AcquiringSberbank\Entity\Setting;
use Blezigen\AcquiringSberbank\Exception\ResponseException;
use Blezigen\AcquiringSberbank\Http\HttpClient;
use Blezigen\AcquiringSberbank\Response\BasicResponse;
use Blezigen\AcquiringSberbank\Response\ResponseReverse;

trait MethodReverse
{

    /**
     * Запрос отмены оплаты заказа
     * @param $orderId
     * @return ResponseReverse
     * @throws ResponseException
     */
    public function reverse($orderId)
    {
        /** @var HttpClient $httpClient */
        $httpClient = $this->httpClient;
        /** @var Setting $setting */
        $setting = $this->setting;

        $params = [
            "orderId" => $orderId
        ];

        /** @var ResponseReverse $response */

        $response = $httpClient->makeRequest(
            "reverse.do",
            $setting->getHttpMethod(),
            $params,
            false,
            ResponseReverse::class
        );

        if ($response->getErrorCode() != 0) {
            throw new ResponseException($response->getErrorCode(), $response->getErrorMessage());
        }

        return $response;
    }
}