<?php
namespace Blezigen\AcquiringSberbank\Methods;

use Blezigen\AcquiringSberbank\Entity\Order;
use Blezigen\AcquiringSberbank\Entity\Setting;
use Blezigen\AcquiringSberbank\Exception\ResponseException;
use Blezigen\AcquiringSberbank\Http\HttpClient;
use Blezigen\AcquiringSberbank\Response\ResponseOrderStatus;

trait MethodGetOrderStatus
{
    /**
     * @param Order $order
     * @return ResponseOrderStatus
     * @throws ResponseException
     */
    public function getOrderStatus($order)
    {
        /** @var HttpClient $httpClient */
        $httpClient = $this->httpClient;
        /** @var Setting $setting */
        $setting = $this->setting;

        $link = "getOrderStatus.do";
        $method = $setting->getHttpMethod();
        $returnClass = ResponseOrderStatus::class;

        $params = [
            "orderId" => $order->getOrderId(),
        ];


        /** @var ResponseOrderStatus $response * */
        $response = $httpClient->makeRequest($link, $method, $params, false, $returnClass);

        if ($response->getErrorCode() != 0) {
            throw new ResponseException($response->getErrorCode(), $response->getErrorMessage());
        }

        return $response;
    }


}