<?php


namespace Blezigen\AcquiringSberbank\Methods;


use Blezigen\AcquiringSberbank\Entity\Setting;
use Blezigen\AcquiringSberbank\Exception\ResponseException;
use Blezigen\AcquiringSberbank\Http\HttpClient;
use Blezigen\AcquiringSberbank\Response\BasicResponse;
use Blezigen\AcquiringSberbank\Response\ResponseDeposit;

trait MethodDeposit
{
    /**
     * Запрoс завершения oплаты заказа
     * @param $orderId
     * @param $amount
     * @return ResponseDeposit
     * @throws ResponseException
     */
    public function deposit($orderId, $amount = 0)
    {
        /** @var HttpClient $httpClient */
        $httpClient = $this->httpClient;
        /** @var Setting $setting */
        $setting = $this->setting;

        $params = [
            "orderId" => $orderId,
            "amount" => $amount
        ];

        /** @var ResponseDeposit $response */

        $response = $httpClient->makeRequest(
            "deposit.do",
            $setting->getHttpMethod(),
            $params,
            false,
            ResponseDeposit::class
        );

        if ($response->getErrorCode() != 0) {
            throw new ResponseException($response->getErrorCode(), $response->getErrorMessage());
        }

        return $response;
    }
}