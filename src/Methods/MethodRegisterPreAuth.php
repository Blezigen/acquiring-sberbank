<?php

namespace Blezigen\AcquiringSberbank\Methods;

use Blezigen\AcquiringSberbank\Entity\Order;
use Blezigen\AcquiringSberbank\Entity\Setting;
use Blezigen\AcquiringSberbank\Exception\ResponseException;
use Blezigen\AcquiringSberbank\Http\HttpClient;
use Blezigen\AcquiringSberbank\Response\ResponseRegister;
use Blezigen\AcquiringSberbank\Response\ResponseRegisterPreAuth;

trait MethodRegisterPreAuth
{
    /**
     * Запрос регистрации заказа c предавторизацией
     * @param Order $order
     * @return ResponseRegisterPreAuth
     * @throws ResponseException
     */
    public function registerPreAuth(Order $order)
    {
        /** @var HttpClient $httpClient */
        $httpClient = $this->httpClient;
        /** @var Setting $setting */
        $setting = $this->setting;

        $link = "registerPreAuth.do";
        $method = $setting->getHttpMethod();
        $returnClass = ResponseRegisterPreAuth::class;

        $params = [
            "orderNumber" => $order->getOrderNumber(),
            "amount" => $order->getAmount(),
            "description" => $order->getDescription(),

            "returnUrl" => $setting->getReturnUrl(),
            "failUrl" => $setting->getFailedUrl(),

            "sessionTimeoutSecs" => $setting->getSessionTimeOutSecond(),
        ];

        if (!empty($order->getParams()))
            $params["jsonParams"] = $order->getJsonParams();

        /** @var ResponseRegisterPreAuth $response * */
        $response = $httpClient->makeRequest($link, $method, $params, false, $returnClass);

        if ($response->getErrorCode() != 0) {
            throw new ResponseException($response->getErrorCode(), $response->getErrorMessage());
        }

        return $response;
    }
}