<?php
namespace Blezigen\AcquiringSberbank\Methods;

use Blezigen\AcquiringSberbank\Entity\Order;
use Blezigen\AcquiringSberbank\Entity\Setting;
use Blezigen\AcquiringSberbank\Exception\ResponseException;
use Blezigen\AcquiringSberbank\Http\HttpClient;
use Blezigen\AcquiringSberbank\Response\ResponseOrderStatusExtended;

trait MethodGetOrderStatusExtended
{
    /**
     * @param Order $order
     * @return ResponseOrderStatusExtended
     * @throws ResponseException
     */
    public function getOrderStatusExtended($order)
    {
        /** @var HttpClient $httpClient */
        $httpClient = $this->httpClient;
        /** @var Setting $setting */
        $setting = $this->setting;

        $link = "getOrderStatusExtended.do";
        $method = $setting->getHttpMethod();
        $returnClass = ResponseOrderStatusExtended::class;

        $params = [
            "orderId" => $order->getOrderId(),
            "orderNumber" => $order->getOrderNumber()
        ];

        /** @var ResponseOrderStatusExtended $response * */
        $response = $httpClient->makeRequest($link, $method, $params, false, $returnClass);

        if ($response->getErrorCode() != 0) {
            throw new ResponseException($response->getErrorCode(), $response->getErrorMessage());
        }

        return $response;
    }
}