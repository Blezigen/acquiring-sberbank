<?php

namespace Blezigen\AcquiringSberbank\Methods;

use Blezigen\AcquiringSberbank\Entity\Setting;
use Blezigen\AcquiringSberbank\Exception\ResponseException;
use Blezigen\AcquiringSberbank\Http\HttpClient;
use Blezigen\AcquiringSberbank\Response\BasicResponse;
use Blezigen\AcquiringSberbank\Response\ResponseRefund;
use Blezigen\AcquiringSberbank\Response\ResponseRegister;
use Blezigen\AcquiringSberbank\Response\ResponseRegisterPreAuth;

trait MethodRefund
{
    /**
     * Запрос возврата средств оплаты заказа
     * @param string $orderId
     * @param int $amount
     * @return ResponseRefund
     * @throws ResponseException
     */
    public function refund($orderId, $amount)
    {
        /** @var HttpClient $httpClient */
        $httpClient = $this->httpClient;
        /** @var Setting $setting */
        $setting = $this->setting;

        $link = "refund.do";
        $method = $setting->getHttpMethod();
        $returnClass = ResponseRefund::class;

        $params = [
            "orderId" => $orderId,
            "amount" => $amount,
        ];

        /** @var ResponseRefund $response * */
        $response = $httpClient->makeRequest($link, $method, $params, false, $returnClass);

        if ($response->getErrorCode() != 0) {
            throw new ResponseException($response->getErrorCode(), $response->getErrorMessage());
        }

        return $response;
    }
}