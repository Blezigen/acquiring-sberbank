<?php


namespace Blezigen\AcquiringSberbank\Type;


use ReflectionClass;

class Enum
{
    /**
     * @param $val
     * @return bool
     */
    public static function isDefinedValue($val)
    {
        $reflect = new ReflectionClass(get_called_class());
        $constArray = $reflect->getConstants();
        return in_array($val, $constArray, true);
    }
}