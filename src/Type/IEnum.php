<?php
namespace Blezigen\AcquiringSberbank\Type;
use ReflectionClass;

/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 04.03.2018
 * Time: 18:12
 */
interface IEnum
{
    /**
     * @param $val
     * @return bool
     */
    public static function isDefinedValue($val);
}