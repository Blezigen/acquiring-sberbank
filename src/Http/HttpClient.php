<?php

namespace Blezigen\AcquiringSberbank\Http;

use Blezigen\AcquiringSberbank\Exception\CurlException;
use Blezigen\AcquiringSberbank\Exception\LimitException;
use Blezigen\AcquiringSberbank\HttpMethod;
use Blezigen\AcquiringSberbank\Response\BasicResponse;

/**
 * Class Client
 * @package Blezigen\Acquiring\Sberbank\Http
 */
class HttpClient
{
    protected $url;
    protected $defaultParameters;

    /**
     * Client constructor.
     *
     * @param string $url Api url
     * @param array $defaultParameters array of parameters
     *
     * @throws \InvalidArgumentException
     */
    public function __construct($url, $defaultParameters = [])
    {
        if (false === stripos($url, 'https://')) {
            throw new \InvalidArgumentException('API schema requires HTTPS protocol');
        }

        $this->url = $url;
        $this->defaultParameters = $defaultParameters;
    }

    /**
     * Make HTTP request
     *
     * @param string $path request url
     * @param string $method (default: 'GET')
     * @param array $parameters (default: array())
     * @param bool $fullPath (default: false)
     *
     * @param string $returnClass
     * @return BasicResponse
     *
     */
    public function makeRequest($path, $method = HttpMethod::GET, array $parameters = [], $fullPath = false, $returnClass = BasicResponse::class)
    {

        if (!HttpMethod::isDefinedValue($method)) {
            throw new \InvalidArgumentException(
                sprintf(
                    'Method "%s" is not valid. Allowed methods are %s',
                    $method,
                    implode(', ', [HttpMethod::GET, HttpMethod::POST])
                )
            );
        }

        $parameters = array_merge($this->defaultParameters, $parameters);

        // Concat full path
        $url = $fullPath ? $path : $this->url . $path;

        // Build link when method is GET
        if (HttpMethod::GET === $method && count($parameters)) {
            $url .= '?' . http_build_query($parameters, '', '&');
        }

        $curlHandler = curl_init();
        curl_setopt($curlHandler, CURLOPT_URL, $url);
        curl_setopt($curlHandler, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curlHandler, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curlHandler, CURLOPT_FAILONERROR, false);
        curl_setopt($curlHandler, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curlHandler, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curlHandler, CURLOPT_TIMEOUT, 30);
        curl_setopt($curlHandler, CURLOPT_CONNECTTIMEOUT, 30);


        if (HttpMethod::POST === $method) {
            curl_setopt($curlHandler, CURLOPT_POST, true);
            curl_setopt($curlHandler, CURLOPT_POSTFIELDS, $parameters);
        }

        $responseBody = curl_exec($curlHandler);
        $statusCode = curl_getinfo($curlHandler, CURLINFO_HTTP_CODE);

        if ($statusCode == 503) {
            throw new LimitException("Service temporarily unavailable.");
        }

        $lastErrorCode = curl_errno($curlHandler);
        $lastErrorMessage = curl_error($curlHandler);

        curl_close($curlHandler);

        if ($lastErrorCode) {
            throw new CurlException($lastErrorCode, $lastErrorMessage);
        }

        if (class_exists($returnClass)) {

            $parent = get_parent_class($returnClass);
            if ($parent != BasicResponse::class)
                throw new \InvalidArgumentException("$returnClass is not a child class " . BasicResponse::class);



            /** @var BasicResponse $temp */

            $temp = new $returnClass();

            $temp->setCode($statusCode);
            $temp->setResponse($responseBody);

            $tempArray = json_decode($responseBody, true);

            $temp->cast($tempArray);

            return $temp;

        } else {
            return new BasicResponse($statusCode, $responseBody);
        }

    }
}