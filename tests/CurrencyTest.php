<?php
namespace Blezigen\AcquiringSberbank\Tests;

use Blezigen\AcquiringSberbank\Currency;
use PHPUnit\Framework\TestCase;

class CurrencyTest extends TestCase
{

    public function testIsDefinedValue()
    {
        $result = Currency::isDefinedValue(Currency::RUB);
        $this->assertEquals($result,true);
    }

    public function testNotIsDefinedValue()
    {
        $result = Currency::isDefinedValue(1000);
        $this->assertEquals($result,false);
    }

}
